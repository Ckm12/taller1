public class Configuracion {
    private static Configuracion instancia = null;
    private String nombreAplicacion;

    private Configuracion() {
        nombreAplicacion = "Mi Aplicación";
    }

    public static Configuracion getInstancia() {
        if (instancia == null) {
            instancia = new Configuracion();
        }
        return instancia;
    }

    public String getNombreAplicacion() {
        return nombreAplicacion;
    }
}